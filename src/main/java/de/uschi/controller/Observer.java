package de.uschi.controller;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class Observer implements PropertyChangeListener
{
    public void propertyChange(PropertyChangeEvent pce) {
        System.out.println("Bei der " + pce.getSource() + " wurde der Parameter \"" +
                pce.getPropertyName() + "\" von \"" + pce.getOldValue() +
                "\" auf \"" + pce.getNewValue() + "\" geaendert.");
    }
}

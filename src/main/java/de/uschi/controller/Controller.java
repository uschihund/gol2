package de.uschi.controller;

import de.uschi.view.*;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Random;

public class Controller extends Observable
{
    int rows, cols, cellSize, speed, generation;

    private GolFrame gui;
    private GolPanel panel;
    private GolCell[][] cells;
    private int[][] previousGeneration, currentGeneration;

    public Controller(int rows, int cols, int cellSize, int speed)
    {
        this.rows = rows;
        this.cols = cols;
        this.cellSize = cellSize;
        this.speed = speed;
        generation = 1;

        gui = new GolFrame(this.rows, this.cols, cellSize);
        panel = new GolPanel();
        cells = new GolCell[rows][cols];

        previousGeneration = new int[rows][cols];

        createCells();
        changeCellColors();

        currentGeneration = new int[rows][cols];

        gui.addPanelToFrame(panel);

        while (currentGeneration != previousGeneration) {

            //previousGeneration = currentGeneration;

            try {
                getNexGeneration();
                changeCellColors();
                generation++;
                if (generation % 50 == 0) gui.setTitle("generation: " + generation);
                Thread.sleep(speed);
            } catch (InterruptedException e) {
            }
        }

        System.out.println("ende: " + generation + " generationen.");

    }

    private void createCells()
    {
        for (int i=0; i< rows; i++) {

            JPanel row = new JPanel();
            row.setLayout(new BoxLayout(row, BoxLayout.LINE_AXIS));
            row.setSize(new Dimension(cols * cellSize, cellSize));
            row.setBackground(Color.BLACK);
            row.setBorder(null);

            for (int j=0; j< cols; j++) {
                GolCell cell = new GolCell(i, j, cellSize);
                cell.setAlive(randomDeadOrAlive());
                cell.setNextColor(cell.isAlive() ? new Color(0, 64, 128) : Color.BLACK);
                addObserver(cell);
                cells[i][j] = cell;
                previousGeneration[i][j] = (cell.isAlive()) ? 1 : 0;
                row.add(cell);
            }
            panel.add(row);
        }

        for (int i=0; i< rows; i++) {
            for (int j = 0; j < cols; j++) {
                cells[i][j].setWillSurvive(willCellSurvive(cells[i][j]));
            }
        }
    }

    public void getNexGeneration()
    {

        for (int i=0; i<rows; i++) {

            for (int j = 0; j < cols; j++) {
                GolCell cell = cells[i][j];
                cell.setAlive(cell.willSurvive());
                cell.setWillSurvive(willCellSurvive(cell));
                setGenerationsSurvived(cell);

                currentGeneration[i][j] = (cell.isAlive()) ? 1 : 0;

                if (!cell.willSurvive()) cell.setNextColor(new Color(16, 16, 16));
                else {
                    cell.setNextColor(getCellColor(cell));
                }
            }

        }
    }

    public boolean randomDeadOrAlive()
    {
        Random rand = new Random();

        int alive = rand.nextInt((1) + 1);

        return (alive == 1);
    }

    private boolean willCellSurvive(GolCell cell)
    {
        if (cell.isAlive() && (getLivingNeighbours(cell) == 2 || getLivingNeighbours(cell) == 3 ) ||
                !cell.isAlive() && (getLivingNeighbours(cell) == 3)) {

            cell.addOneToGenerationsAlive();

            return true;
        } else {
            cell.setGenerationsAlive(0);
            return false;
        }
    }

    private void setGenerationsSurvived(GolCell cell)
    {
        int generationsSurvived = cell.getGenerationsAlive();

        generationsSurvived += (cell.isAlive() && cell.willSurvive()) ? 1 : 0;

        cell.setGenerationsAlive(generationsSurvived);
    }

    private int getLivingNeighbours(GolCell cell) {
        int livingNeighbours = 0;

        if (cell.getRow() != 0)
            livingNeighbours += getLivingNeighboursInRow(cell, cell.getRow() - 1);

        livingNeighbours += getLivingNeighboursInRow(cell, cell.getRow());

        if (cell.getRow() != rows -1)
            livingNeighbours += getLivingNeighboursInRow(cell, cell.getRow() + 1);

        return livingNeighbours;
    }

    private int getLivingNeighboursInRow(GolCell cell, int row) {
        int livingNeighboursInRow = 0;

        if (cell.getCol() != 0) {
            livingNeighboursInRow += cells[row][cell.getCol() - 1].isAlive() ? 1 : 0;
        }

        if (cell.getRow() != row) {
            livingNeighboursInRow += cells[row][cell.getCol()].isAlive() ? 1 : 0;
        }

        if (cell.getCol() < cols - 1) {
            livingNeighboursInRow += cells[row][cell.getCol() + 1].isAlive() ? 1 : 0;
        }

        return livingNeighboursInRow;
    }

    private Color getCellColor(GolCell cell) {

        int colorValue =  ((cell.getGenerationsAlive() * 5) + 128 >= 255) ?
                127 :
                (cell.getGenerationsAlive() * 5);

        Color color = (cell.isAlive()) ?
                new Color(0 + colorValue, 64 + colorValue, 128 + colorValue) :
                new Color(16, 16, 16);

        return color;
    }

    public void changeCellColors()
    {
        setChanged();
        notifyObservers();
    }

}

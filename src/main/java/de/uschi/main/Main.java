package de.uschi.main;

import de.uschi.controller.Controller;

public class Main
{
    public static void main(String[] args)
    {
        System.setProperty("sun.java2d.transaccel", "True");
        System.setProperty("sun.java2d.opengl", "true");

        int boardRows = 10;
        int boardCols = 10;
        int cellSize = 10;
        int fps = 24;

        int speed = 1000/fps;

        run(boardRows, boardCols, cellSize, speed);
    }

    public static void run(int boardRows, int boardCols, int cellSize, int speed)
    {
        Controller controller = new Controller(boardRows, boardCols, cellSize, speed);
    }
}

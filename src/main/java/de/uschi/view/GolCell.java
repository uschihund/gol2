package de.uschi.view;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class GolCell extends JPanel implements Observer
{
    private boolean alive, willSurvive;
    private int row, col, cellSize, generationsAlive;
    private Color nextColor;

    public GolCell(int row, int col, int size)
    {
        this.row = row;
        this.col = col;
        this.cellSize = size;

        setPreferredSize(new Dimension(size, size));
        setBorder(new LineBorder(Color.BLACK, 1));
        setDoubleBuffered(true);
    }

    public void changeColor() {
        if (getBackground() != nextColor) {
            setBackground(nextColor);
            repaint();
            revalidate();
        }
    }

    public void update(Observable observable, Object color)
    {
        changeColor();
    }

    public boolean isAlive()
    {
        return alive;
    }

    public void setAlive(boolean alive)
    {
        this.alive = alive;
    }

    public boolean willSurvive()
    {
        return willSurvive;
    }

    public void setWillSurvive(boolean willSurvive)
    {
        this.willSurvive = willSurvive;
    }

    public int getGenerationsAlive()
    {
        return generationsAlive;
    }

    public void setGenerationsAlive(int generationsAlive)
    {
        this.generationsAlive = generationsAlive;
    }
    public void addOneToGenerationsAlive()
    {
        this.generationsAlive++;
    }

    public Color getNextColor()
    {
        return nextColor;
    }

    public void setNextColor(Color nextColor)
    {
        this.nextColor = nextColor;
    }

    public int getRow()
    {
        return row;
    }

    public void setRow(int row)
    {
        this.row = row;
    }

    public int getCol()
    {
        return col;
    }

    public void setCol(int col)
    {
        this.col = col;
    }

    public int getCellSize()
    {
        return cellSize;
    }

    public void setCellSize(int cellSize)
    {
        this.cellSize = cellSize;
    }
}

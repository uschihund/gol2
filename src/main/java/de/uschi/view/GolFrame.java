package de.uschi.view;

import javax.swing.*;
import java.awt.*;

public class GolFrame extends JFrame
{
    private int rows, cols, cellSize;

    public GolFrame(int rows, int cols, int cellSize)
    {
        this.rows = rows;
        this.cols = cols;
        this.cellSize = cellSize;
        setUp();
    }

    private void setUp()
    {
        setTitle("game of life - Peter Senkel");
        setSize(new Dimension(rows * cellSize, cols * cellSize));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public void addPanelToFrame(GolPanel panel)
    {
        add(panel);
    }
}

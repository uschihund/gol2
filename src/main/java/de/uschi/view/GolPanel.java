package de.uschi.view;

import javax.swing.*;
import java.awt.*;

public class GolPanel extends JPanel
{
    public GolPanel()
    {
        setBackground(Color.BLACK);
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        setDoubleBuffered(true);
    }
}
